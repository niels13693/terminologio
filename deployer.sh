#Création de la base de donnée:
apt -y install jq
username=$(jq -r ".username" conf.json)
sed -i -e "s/{username}/${username}/g" database.sql
password=$(jq -r ".password" conf.json)
sed -i -e "s/{password}/${password}/g" database.sql
dbname=$(jq -r ".dbname" conf.json)
sed -i -e "s/{databasename}/${dbname}/g" database.sql
mysql -u root -p < database.sql
echo "exit"
#deplacement des fichiers:
path=$(jq -r ".path" conf.json)
cp -vr * "$path"
rm "${path}deployer.sh"
rm "${path}index.html"
#attribution des droits sur le dossier images:
chown -R root:www-data "${path}concepts/images/"
chmod -R g+rw "${path}concepts/images/"
#Création du compte administrateur:
cd ${path}
php createAdmin.php
echo "done"
