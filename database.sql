#Création de l'utilisateur:

CREATE OR REPLACE USER {username} IDENTIFIED BY '{password}';


#Création de la base de donnée:

CREATE OR REPLACE DATABASE {databasename};

GRANT ALL PRIVILEGES ON {databasename}.* TO {username};
#Selection de la base de donnée:

use {databasename};

#Création des tables:

CREATE TABLE user (
                      mail VARCHAR(50),
                      username VARCHAR(20),
                      password VARCHAR(255),
                      native_language VARCHAR(20),
                      cookie_id VARCHAR(255),
                      PRIMARY KEY (mail)
)ENGINE = INNODB;

CREATE TABLE concept (
                         name VARCHAR(20),
                         image VARCHAR(50),
                         user_mail VARCHAR(50),
                         id INT UNIQUE AUTO_INCREMENT,
                         PRIMARY KEY (name,user_mail),
                         FOREIGN KEY (user_mail)
                             REFERENCES user(mail)
                             ON DELETE CASCADE
)ENGINE = INNODB;

CREATE TABLE terminology (
                             description VARCHAR(200),
                             x_coord FLOAT,
                             y_coord FLOAT,
                             x_number_coord FLOAT,
                             y_number_coord FLOAT,
                             concept_name VARCHAR(20),
                             user_mail VARCHAR(50),
                             PRIMARY KEY (x_coord,y_coord,concept_name,user_mail),
                             FOREIGN KEY (concept_name,user_mail)
                                 REFERENCES  concept(name,user_mail)
                                 ON DELETE CASCADE
)ENGINE = INNODB;

CREATE TABLE banlist (
    mail VARCHAR(50) PRIMARY KEY
)ENGINE = INNODB;