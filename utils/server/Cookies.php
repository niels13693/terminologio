<?php

namespace utils\server;
require("Connectdb.php");

use PDO;

class Cookies {

    public static function verifyConnection($connectedPage,$pathToSrc) {
        //Récupération des cookies:
        if(isset($_COOKIE["login"])) {
            $id = $_COOKIE['login'];
        }
        //Connection à la bdd:

        $connect = Connectdb::log($pathToSrc);

        //Récuperation de l'id de l'utilisateur:

        $getUserId = $connect->prepare('SELECT mail FROM user WHERE cookie_id = :cookie_id');
        $getUserId->bindParam(':cookie_id',$id);
        $resUserId = $getUserId->execute();
        if (!$resUserId) {
            echo 'query execution error';
            die();
        }
        if ($getUserId->rowCount() <= 0) {
            if ($connectedPage) {
                header('Location: ../../index.php');
                exit;
            } else {
                return [$connect,false];
            }

        } else {
            if (!$connectedPage) {
                return [$connect,true];
            } else {
                return $connect;
            }
        }
    }
}