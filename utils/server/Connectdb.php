<?php

namespace utils\server;

use PDO;
use PDOException;

class Connectdb {

    public static function log($pathToSrc) {
        $json = file_get_contents($pathToSrc . "conf.json");
        $data = json_decode($json,true);
        $dbname = $data["dbname"];
        $port = 3306;
        $host = $data["host"];
        $username = $data["username"];
        $password = $data["password"];
        try {
            $connect = new PDO('mysql:host=' . $host .';port=' . $port . ';dbname=' . $dbname,$username,$password);
        } catch (PDOException $e) {
            echo 'PDO creation error : ' . $e->getMessage();
            die();
        }
        return $connect;
    }

}