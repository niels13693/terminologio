<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');

use utils\server\Connectdb;
require("Connectdb.php");


//Récuperation des cookies:
$name = $_REQUEST['name'];
$mail = $_REQUEST['mail'];

//Connexion à la bdd:

$connect = Connectdb::log("../../");

//Verification du nom:

$getName = $connect->prepare("SELECT name FROM concept WHERE user_mail = :mail and name = :name");
$getName->bindParam(":name",$name);
$getName->bindParam(":mail",$mail);
$resName = $getName->execute();
if (!$resName) {
    echo 'query execution error';
    die();
}

if ($getName->rowCount() > 0) {
    echo "present";
} else {
    echo "absent";
}
exit;