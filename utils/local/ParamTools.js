import Terminology from "./Terminology.js";

export default class ParamTools {

    /**
     * Vérifie que param est du type String sinon throw une erreur avec functionName dans le message d'erreur.
     * @param param paramètre à vérifier.
     * @param functionName nom de la fonction où la vérification à été appelée.
     */
    static verifyTypeStringParam(param,functionName) {
        if (typeof functionName !== typeof String()) {
            throw new Error("verifyTypeStringParam: param - invalid type");
        }
        if (typeof param !== typeof String()) {
            throw new Error(functionName + ": param - invalid type");
        }
    }

    /**
     * Vérifie que param est du type HTMLParagraphElement sinon throw une erreur avec functionName
     * dans le message d'erreur.
     * @param param paramètre à vérifier.
     * @param functionName nom de la fonction où la vérification à été appelée.
     */
    static verifyTypeHTMLPParam(param,functionName) {
        this.verifyTypeStringParam(functionName,"verifyTypeHTMLPParam");
        if (param.nodeName !== document.createElement("p").nodeName) {
            throw new Error(functionName + ": param - invalid type");
        }
    }

    /**
     * Vérifie que param est du type HTMLInputElement sinon throw une erreur avec functionName
     * dans le message d'erreur.
     * @param param paramètre à vérifier.
     * @param functionName nom de la fonction où la vérification à été appelée.
     */
    static verifyTypeHTMLInputParam(param,functionName) {
        this.verifyTypeStringParam(functionName,"verifyTypeHTMLInputParam");
        if (param.nodeName !== document.createElement("input").nodeName) {
            throw new Error(functionName + ": param - invalid type");
        }
    }

    /**
     * Vérifie que param est du type HTMLInputElement sinon throw une erreur avec functionName
     * dans le message d'erreur.
     * @param param paramètre à vérifier.
     * @param functionName nom de la fonction où la vérification à été appelée.
     */
    static verifyTypeHTMLDivParam(param,functionName) {
        this.verifyTypeStringParam(functionName,"verifyTypeHTMLDivParam");
        if (param.nodeName !== document.createElement("div").nodeName) {
            throw new Error(functionName + ": param - invalid type");
        }
    }

    /**
     * Vérifie que param est du type Svg sinon throw une erreur avec functionName
     * dans le message d'erreur.
     * @param param paramètre à vérifier.
     * @param functionName nom de la fonction où la vérification à été appelée.
     */
    static verifyTypeSvgParam(param,functionName) {
        this.verifyTypeStringParam(functionName,"verifyTypeSvgParam");
        if (param.nodeName !== document.createElementNS("http://www.w3.org/2000/svg","svg").nodeName) {
            throw new Error(functionName + ": param - invalid type");
        }
    }

    /**
     * Vérifie que param est du type Number sinon throw une erreur avec functionName
     * dans le message d'erreur.
     * @param param paramètre à vérifier.
     * @param functionName nom de la fonction où la vérification à été appelée.
     */
    static verifyTypeNumberParam(param,functionName) {
        this.verifyTypeStringParam(functionName,"verifyTypeSvgParam");
        if (typeof param !== typeof Number()) {
            throw new Error(functionName + ": param - invalid type");
        }
    }

    /**
     * Vérifie que param est du type Terminology sinon throw une erreur avec functionName
     * dans le message d'erreur.
     * @param param paramètre à vérifier.
     * @param functionName nom de la fonction où la vérification à été appelée.
     */
    static verifyTypeTerminologyParam(param,functionName) {
        this.verifyTypeStringParam(functionName,"verifyTypeSvgParam");
        if (typeof param !== typeof new Terminology(0)) {
            throw new Error(functionName + ": param - invalid type");
        }
    }
}