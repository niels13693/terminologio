import ParamTools from "./ParamTools.js";

export default class CookieTools {

    static parseCookieValue(name) {
        ParamTools.verifyTypeStringParam(name,"parseCookieValue");
        const cookieString = decodeURIComponent(document.cookie);
        const sliced = cookieString.split(";");
        for (let i = 0; i < sliced.length; ++i) {
            const dataCookie = sliced[i].split("=");
            const cookieName = dataCookie[0].replace(/\s/g, "");
            const value = dataCookie[1];
            if (cookieName === name) {
                return value;
            }
        }
        return null;
    }
}