import ParamTools from "./ParamTools.js";

export default class Concept {

    //Attributs:

    #image;
    #id;

    //constructeur:

    constructor() {
        this.#image = new Image();
        this.#id = undefined;
    }

    //Requetes:

    get image() {
        return this.#image;
    }

    get id() {
        return this.#id;
    }

    //Commandes:

    removeTerm(id,source,svg) {
        ParamTools.verifyTypeNumberParam(id,"removeTerm");
        ParamTools.verifyTypeHTMLDivParam(source,"removeTerm");
        ParamTools.verifyTypeSvgParam(svg,"removeTerm")
        //Suppression de la ligne:
        svg.removeChild(document.getElementById("line" + id));
        //Suppression du texte:
        svg.removeChild(document.getElementById("num" + id));
        //Rennomage des autres texte sur le svg:
        for (let i = id + 1;i <= document.getElementsByClassName("term").length;++i) {
            const num = document.getElementById("num" + i);
            num.setAttributeNS(null,"id","num" + (i - 1));
            num.innerHTML = (i - 1).toString();
            const line = document.getElementById("line" + i);
            line.setAttributeNS(null,"id","line" +  (i - 1));
            const div = document.getElementById("term" + i);
            div.id = "term" + (i - 1);
            const label = document.getElementById("label" + i);
            label.id = "label" + (i - 1);
            label.innerText = (i - 1).toString();
            label.setAttribute("for","desc" + (i - 1));
            const input = document.getElementById("desc" + i);
            input.name = "descriptionTerm" + (i - 1);
            input.placeholder = "Description de la terminologie " + (i - 1);
            input.id = "desc" + (i - 1);
            const deleteButton = document.getElementById("deleteButton" + i);
            deleteButton.id = "deleteButton" + (i - 1);
            const x_coord = document.getElementById("x_coord" + i);
            x_coord.id = "x_coord" + (i - 1);
            x_coord.name = "x_coord" + (i - 1);
            const y_coord = document.getElementById("y_coord" + i);
            y_coord.id = "y_coord" + (i - 1);
            y_coord.name = "y_coord" + (i - 1);
            const x_number_coord = document.getElementById("x_number_coord" + i);
            x_number_coord.id = "x_number_coord" + (i - 1);
            x_number_coord.name = "x_number_coord" + (i - 1);
            const y_number_coord = document.getElementById("y_number_coord" + i);
            y_number_coord.id = "y_number_coord" + (i - 1);
            y_number_coord.name = "y_number_coord" + (i - 1);
        }
        source.removeChild(document.getElementById("term" + id));

    }

    addTerm(id,term,dest) {
        ParamTools.verifyTypeNumberParam(id,"addTerm");
        ParamTools.verifyTypeTerminologyParam(term,"addTerm");
        ParamTools.verifyTypeHTMLDivParam(dest,"addTerm");
        //Ajout au document HTML:
        //Création du div:
        const div = document.createElement("div");
        div.className = "term";
        div.id = "term" + id;
        //label:
        const label = document.createElement("label");
        label.innerText = id.toString();
        label.setAttribute("for","desc" + id);
        label.id = "label" + id;
        div.appendChild(label);
        //input:
        const input = document.createElement("input");
        input.type = "text";
        input.name = "descriptionTerm" + id;
        input.placeholder = "Description de la terminologie " + id;
        input.id = "desc" + id;
        input.minLength = 2;
        input.maxLength = 200;
        div.appendChild(input);
        //delete button:
        const deleteButton = document.createElement("button");
        deleteButton.className = "deleteButton";
        deleteButton.id = "deleteButton" + id;
        deleteButton.type = "button";
        deleteButton.innerText = "x";
        deleteButton.addEventListener("click",(e) => {
            const termId = Number(e.target.id.charAt(e.target.id.length - 1));
            this.removeTerm(termId,dest,document.getElementById("svg"));
        });
        div.appendChild(deleteButton);
        let hidden = document.createElement("input");
        hidden.id = "x_coord" + id;
        hidden.name = "x_coord" + id;
        hidden.type = "hidden";
        hidden.value = term.x1.toString();
        div.appendChild(hidden);
        hidden = document.createElement("input");
        hidden.id = "y_coord" + id;
        hidden.name = "y_coord" + id;
        hidden.type = "hidden";
        hidden.value = term.y1.toString();
        div.appendChild(hidden);
        hidden = document.createElement("input");
        hidden.id = "x_number_coord" + id;
        hidden.name = "x_number_coord" + id;
        hidden.type = "hidden";
        hidden.value = term.x2.toString();
        div.appendChild(hidden);
        hidden = document.createElement("input");
        hidden.id = "y_number_coord" + id;
        hidden.name = "y_number_coord" + id;
        hidden.type = "hidden";
        hidden.value = term.y2.toString();
        div.appendChild(hidden);
        dest.insertBefore(div,document.getElementById("addButton"));
    }

    async loadImageFromServer(id, pathToSrc) {
        ParamTools.verifyTypeNumberParam(id, "loadImageFromServer");
        ParamTools.verifyTypeStringParam(pathToSrc, "loadImageFromServer");
        return new Promise(async (resolve) => {
            //Requete XMLHttp:
            const request = new XMLHttpRequest();
            const url = pathToSrc + "concepts/database/getImage.php?id=" + id;
            request.open("GET", url);
            request.send();
            const reponse = await fetch(url);
            if (reponse.ok) {
                const encodedImage = await reponse.text();
                const image = new Image();
                image.addEventListener("load", () => {
                    this.#image = image;
                    resolve(image);
                });
                image.src = encodedImage;
            }
        });
    }

    loadImageFromFile(source) {
        ParamTools.verifyTypeHTMLInputParam(source,"loadImageFromFile");
        return new Promise((resolve) => {
            //Récuperation du fichier:
            const file = source.files[0];
            //FileReader:
            const fileReader = new FileReader();
            fileReader.addEventListener("load",() => {
                //Image:
                const image = new Image();
                image.addEventListener("load",() => {
                    this.#image = image;
                    resolve(image);
                },false);
                image.src = fileReader.result.toString();
            },false);
            fileReader.readAsDataURL(file);
        });
    }

    drawImage(dest) {
        ParamTools.verifyTypeHTMLDivParam(dest,"drawImage");
        //Redimension de l'image:
        const ratio = Math.ceil(Math.max(this.image.width, this.image.height) / 600);
        const height = Math.ceil(this.image.height / ratio).toString();
        const width = Math.ceil(this.image.width / ratio).toString();
        //image:
        const image = document.createElementNS("http://www.w3.org/2000/svg", "image");
        image.setAttributeNS(null,"href",this.image.src);
        image.setAttributeNS(null,"height",height);
        image.setAttributeNS(null,"width",width);
        //svg:
        const svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
        svg.setAttributeNS(null,"height",height);
        svg.setAttributeNS(null,"width",width);
        svg.setAttributeNS(null,"id","svg");
        svg.appendChild(image);
        dest.appendChild(svg);
    }

    drawLinesFromHTML(dest,typeSource) {
        ParamTools.verifyTypeSvgParam(dest,"drawLinesFromHTML");
        ParamTools.verifyTypeStringParam(typeSource,"drawLinesFromHTML");
        const max = document.getElementsByClassName("term").length;
        for (let i = 1;i <= max;++i) {
            //line:
            let x1 = "";
            let y1 = "";
            let x2 = "";
            let y2 = "";
            if (typeSource === "p") {
                x1 = document.getElementById("x_coord" + i).innerText;
                y1 = document.getElementById("y_coord" + i).innerText;
                x2 = document.getElementById("x_number_coord" + i).innerText;
                y2 = document.getElementById("y_number_coord" + i).innerText;
            } else {
                x1 = document.getElementById("x_coord" + i).value;
                y1 = document.getElementById("y_coord" + i).value;
                x2 = document.getElementById("x_number_coord" + i).value;
                y2 = document.getElementById("y_number_coord" + i).value;
            }
            const line = document.createElementNS("http://www.w3.org/2000/svg","line");
            line.setAttributeNS(null,"x1",x1);
            line.setAttributeNS(null,"x2",x2);
            line.setAttributeNS(null,"y1",y1);
            line.setAttributeNS(null,"y2",y2);
            line.setAttributeNS(null,"id","line" + i);
            line.setAttributeNS(null,"stroke","black");
            dest.appendChild(line);
            //texte:
            const text = document.createElementNS("http://www.w3.org/2000/svg","text");
            const textX = (Number(x2) + 5).toString();
            const textY = (Number(y2) + 5).toString()
            text.setAttributeNS(null,"x",textX);
            text.setAttributeNS(null,"y",textY);
            text.setAttributeNS(null,"id","num" + i);
            text.innerHTML = i.toString();
            dest.appendChild(text);
        }
    }
}

