import ParamTools from "./ParamTools.js";

export default class Terminology {

    //Attributs:

    #x1;
    #y1;
    #x2;
    #y2;
    #id;

    constructor(id) {
        ParamTools.verifyTypeNumberParam(id,"Terminology");
        this.#x1 = null;
        this.#y1 = null;
        this.#x2 = null;
        this.#y2 = null;
        this.#id = id;
    }

    //requetes:


    get x1() {
        return this.#x1;
    }

    get x2() {
        return this.#x2;
    }

    get y1() {
        return this.#y1;
    }

    get y2() {
        return this.#y2;
    }

    get id() {
        return this.#id;
    }

    //Commandes:

    draw(dest) {
        ParamTools.verifyTypeSvgParam(dest,"draw");
        //line:
        const line = document.createElementNS("http://www.w3.org/2000/svg","line");
        line.setAttributeNS(null,"x1",this.x1);
        line.setAttributeNS(null,"x2",this.x2);
        line.setAttributeNS(null,"y1",this.y1);
        line.setAttributeNS(null,"y2",this.y2);
        line.setAttributeNS(null,"id","line" +  this.id);
        line.setAttributeNS(null,"stroke","black");
        dest.appendChild(line);
    }

    drawId(dest) {
        ParamTools.verifyTypeSvgParam(dest,"drawId");
        //text:
        const text = document.createElementNS("http://www.w3.org/2000/svg","text");
        text.setAttributeNS(null,"x",this.x2 + 5);
        text.setAttributeNS(null,"y",this.y2 + 5);
        text.setAttributeNS(null,"id","num" + this.id);
        text.innerHTML = this.id;
        dest.appendChild(text);
    }

    setFirstPoint(source) {
        ParamTools.verifyTypeSvgParam(source,"setFirstPoint");
        const self = this;
        return new Promise((resolve) => {
            source.addEventListener("click", function firstPoint(e) {
                e.currentTarget.removeEventListener(e.type, firstPoint);
                const r = e.target.getBoundingClientRect();
                self.#x1 = e.clientX - r.left;
                self.#y1 = e.clientY - r.top;
                resolve(true);
            });
        });
    }

    setSecondPoint(source) {
        ParamTools.verifyTypeSvgParam(source,"setSecondPoint");
        const self = this;
        return new Promise((resolve) => {
            source.addEventListener("click", function secondPoint(e) {
                e.currentTarget.removeEventListener(e.type, secondPoint);
                const r = e.target.getBoundingClientRect();
                self.#x2 = e.clientX - r.left;
                self.#y2 = e.clientY - r.top;
                resolve(true);
            });
        })
    }
}