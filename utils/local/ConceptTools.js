import ParamTools from "./ParamTools.js";

export default class ConceptTools {

    /**
     * Fonction pour vérifier si l'utilisateur a déja un concept avec le même nom que celui entré et
     * retourne true si le mot est present et false sinon.
     * @param name nom à verifier (String)
     * @param mail mail de l'utilisateur (String)
     * @param pathToSrc chemin vers le fichier racine du site web.
     */
    static async verifyNameOnDB(name, mail, pathToSrc) {
        ParamTools.verifyTypeStringParam(name, "verifyNameOnDB");
        ParamTools.verifyTypeStringParam(mail, "verifyNameOnDB");
        ParamTools.verifyTypeStringParam(pathToSrc,"verifyNameOnDB");
        const request = new XMLHttpRequest();
        const url = pathToSrc + "utils/server/verifyName.php?name=" + name + "&mail=" + mail;
        request.open("GET", url);
        request.send();
        const reponse = await fetch(url);
        if(reponse.ok) {
            return (await reponse.text()) === "present";
        }
    }
}