<?php

    error_reporting(E_ALL);
    ini_set('display_errors', 'On');

    use utils\server\Cookies;
    require("utils/server/Cookies.php");

    $res = Cookies::verifyConnection(false,"");
    $connect = $res[0];
    $connected = $res[1];

    //Récuperation du numero de la page:
    $nbPage = $_GET["page"];
    if(!isset($nbPage)) {
        header("Location: index.php?page=1");
        exit;
    }
    //Verification de l'existence de la page:
    $checkPageNumber = $connect->prepare("SELECT count(*) FROM concept");
    $resCheck = $checkPageNumber->execute();
    if (!$resCheck) {
        echo 'query execution error';
        die();
    }
    $nbPagePossible = ceil($checkPageNumber->fetch()[0] / 10);
    if (($nbPage > $nbPagePossible or $nbPage <= 0) and $nbPagePossible != 0) {
        header("Location: index.php?page=1");
        exit;
    }
    //Récuperation des concepts de la page:
    $offset = ($nbPage - 1) * 10;
    $getConceptsPage = $connect->prepare("SELECT name,id 
                                                FROM concept
                                                ORDER BY name
                                                LIMIT :offset,10;");
    $getConceptsPage->bindParam(":offset",$offset,PDO::PARAM_INT);
    $resPage = $getConceptsPage->execute();
    if (!$resPage) {
        echo 'query execution error';
        die();
    }
    $dataPage = $getConceptsPage->fetchAll(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Terminologio - home</title>
    <link rel="stylesheet" href="assets/style/index.css">
    <script src="assets/scripts/myConcepts.js"></script>
</head>
<body>
    <header>
        <h1>Terminologio</h1>
        <div>
            <?php
                if ($connected) {
                    echo "<a href='concepts/view/myConcepts.php'>Mes concepts</a>
                          <a href='concepts/edit/addConcept.php'>Ajouter</a>
                          <a href='users/logout.php'>Déconnexion</a>";
                } else {
                    echo "<a href='users/login.html'>Connexion</a>
                          <a href='users/signup.html'>Inscription</a>";
                }
            ?>

        </div>
    </header>
    <div id="conceptPage">
        <?php
        foreach ($dataPage as $concept) {
            echo "<div id='concept" . $concept['id'] . "' class='concept'>
                    <script>getImage(" . $concept['id'] . ")</script>  
                    <p id='name" . $concept['id'] . "' class='name'>" . $concept['name'] . "</p>
                    <a href='concepts/view/viewConcept.php?id=" . $concept['id'] . "' id='a" . $concept['id'] . "'>
                        <img src='' alt='" . $concept['name'] . "' id='img" . $concept['id'] . "'>
                    </a>
              </div>";
        }
        ?>
    </div>
    <div id="bottomPage">
        <button id="predPage" type="button">
            <a href="index.php?page=<?php echo $nbPage - 1;?>"><</a>
        </button>
        <p><?php echo $nbPage . " / ";
                if ($nbPagePossible > 0) {
                    echo $nbPagePossible;
                } else {
                    echo 1;
                }?></p>
        <button id="nextPage" type="button">
            <a href="index.php?page=<?php echo $nbPage + 1;?>">></a></button>
    </div>
</body>
<script src="assets/scripts/myConcepts.js"></script>
</html>