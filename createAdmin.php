<?php

use utils\server\Connectdb;
require("utils/server/Connectdb.php");

//Récuperation des informations:

$json = file_get_contents("conf.json");
$data = json_decode($json,true);

$adminName = $data["admin_username"];
$adminPassword = password_hash($data["admin_password"],PASSWORD_DEFAULT);
$adminMail = "admin@terminologio";
$cookie_id = password_hash(time() . $adminMail,PASSWORD_DEFAULT);
$adminLanguage = "fr";

//Connexion à la bdd:

$connect = Connectdb::log("");

//Inscription de l'administateur:

$signupAdmin = $connect->prepare("INSERT INTO user(mail,username,password,native_language,cookie_id) values 
                                   (:mail,:username,:password,:language,:cookie)");
$signupAdmin->bindParam(":mail",$adminMail);
$signupAdmin->bindParam(":username",$adminName);
$signupAdmin->bindParam(":password",$adminPassword);
$signupAdmin->bindParam(":language",$adminLanguage);
$signupAdmin->bindParam(":cookie",$cookie_id);
$resAdmin = $signupAdmin->execute();
if (!$resAdmin) {
    echo 'query execution error';
    die();
}

exit;