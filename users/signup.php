<?php

error_reporting(E_ALL);
ini_set('display_errors', 'On');

use utils\server\Connectdb;
require("../utils/server/Connectdb.php");

//Récupération des informations:

$mail = $_POST['user_mail'];
$username = $_POST['username'];
$password = password_hash($_POST['password'],PASSWORD_DEFAULT);
$language = $_POST['language'];

//Connection à la bdd:

$connect = Connectdb::log("../");

//Vérification de la présence de l'utilisateur dans la base de donnée:

$user_check = $connect->prepare('SELECT mail FROM user WHERE mail = :mail');
$user_check->bindParam(':mail',$mail);
$res_check = $user_check->execute();
if (!$res_check) {
    echo 'query execution error';
    die();
}
if ($user_check->rowCount() > 0) {
    header('Location: signup.html?mail=found');
    exit;
}

//Vérification de l'état du ban du mail:
$checkBanList = $connect->prepare("SELECT mail FROM banlist WHERE mail = :mail");
$checkBanList->bindParam(":mail",$mail);
$resBan = $checkBanList->execute();
if (!$resBan) {
    echo 'query execution error';
    die();
}

if ($checkBanList->rowCount() > 0) {
    header("Location: signup.html?ban=true");
    exit;
}

// Inscription de l'utilisateur:

$signup = $connect->prepare('INSERT INTO user(mail,username,password,native_language,cookie_id) values 
                                   (:mail,:username,:password,:language,:cookie)');
$signup->bindParam(':mail',$mail,PDO::PARAM_STR,strlen($mail));
$signup->bindParam(':username',$username,PDO::PARAM_STR,strlen($username));
$signup->bindParam(':password',$password,PDO::PARAM_STR,strlen($password));
$signup->bindParam(':language',$language,PDO::PARAM_STR,strlen($language));
$cookie_id = password_hash(time() . $mail,PASSWORD_DEFAULT);
$signup->bindParam(':cookie',$cookie_id,PDO::PARAM_STR,strlen($cookie_id));
$res_signup = $signup->execute();
if (!$res_signup) {
    echo 'query execution error';
    die();
}

//Création du cookie:
$time = time() + 60*60*24*7;
setcookie('login',$cookie_id,$time,'/','192.168.76.76');
setcookie('mail',$mail,$time,'/','192.168.76.76');

header('Location: ../index.php');
exit;