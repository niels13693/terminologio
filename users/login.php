<?php

error_reporting(E_ALL);
ini_set('display_errors', 'On');

use utils\server\Connectdb;
require("../utils/server/Connectdb.php");

//Récupération des informations:

$mail = $_POST['user_mail'];

//connection a la bdd:

$connect = Connectdb::log("../");

//Récupération du mot de passe stocké en bdd:

$getPwd = $connect->prepare('SELECT password FROM user WHERE mail = :mail');
$getPwd->bindParam(':mail',$mail,PDO::PARAM_STR,strlen($mail));
$resPwd = $getPwd->execute();
if (!$resPwd) {
    echo 'query execution error';
    die();
}
if ($getPwd->rowCount() == 0) {
    header('Location: login.html?mail=not_found');
    exit;
} else if ($getPwd->rowCount() > 1) {
    echo 'error database';
    die();
}

//Vérification du mot de passe:

$hashedPwd = $getPwd->fetch(PDO::FETCH_ASSOC)['password'];
$validPwd = password_verify($_POST['password'],$hashedPwd);
if ($validPwd) {

    //Création du cookie:

    $getCookieId = $connect->prepare('SELECT cookie_id FROM user WHERE mail = :mail');
    $getCookieId->bindParam(':mail',$mail,PDO::PARAM_STR,strlen($mail));
    $resCookie = $getCookieId->execute();
    if (!$resPwd) {
        echo 'query execution error';
        die();
    }
    $cookieId = $getCookieId->fetch(PDO::FETCH_ASSOC)['cookie_id'];
    $time = time() + (60*60*24*7);
    setcookie('login',$cookieId,$time,'/','192.168.76.76');
    setcookie('mail',$mail,$time,'/','192.168.76.76');
    $connect = null;
    header('Location: ../index.php');
} else {
    $connect = null;
    header('Location: login.html?pwd=false');
}
exit;