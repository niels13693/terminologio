<?php

error_reporting(E_ALL);
ini_set('display_errors', 'On');

use utils\server\Connectdb;
require("../utils/server/Connectdb.php");

//Récuperation de l'ID:
$id = $_GET["id"];

$connect = Connectdb::log("../");

//Récuperation du mail:
$getMail = $connect->prepare("SELECT user_mail FROM concept WHERE id = :id");
$getMail->bindParam(":id",$id,PDO::PARAM_INT);
$resMail = $getMail->execute();
if (!$resMail) {
    echo 'query execution error';
    die();
}

$mail = $getMail->fetch(PDO::FETCH_ASSOC)["user_mail"];
//Suppression du compte de l'utilisateur:
$deleteAccount = $connect->prepare("DELETE FROM user WHERE mail = :mail");
$deleteAccount->bindParam(":mail",$mail);
$resDelete = $deleteAccount->execute();
if (!$resDelete) {
    echo 'query execution error';
    die();
}

//Ajout du mail à la banlist:
$addToBanList = $connect->prepare("INSERT INTO banlist(mail) VALUES (:mail)");
$addToBanList->bindParam(":mail",$mail);
$resBan = $addToBanList->execute();
if (!$resBan) {
    echo 'query execution error';
    die();
}

//Retour au site:
header("Location: ../index.php");
exit;