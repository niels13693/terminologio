<?php

error_reporting(E_ALL);
ini_set('display_errors', 'On');

use utils\server\Cookies;
require("../../utils/server/Cookies.php");

$connect = Cookies::verifyConnection(true,"../../");
$mail = $_COOKIE["mail"];

//Récuperation des concepts de l'utilisateur:
$getConcept = $connect->prepare("SELECT name,id FROM concept WHERE user_mail = :mail");
$getConcept->bindParam(":mail",$mail);
$resConcept = $getConcept->execute();
if (!$resConcept) {
    echo 'query execution error';
    die();
}
$dataConcepts = $getConcept->fetchAll(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Terminologio - Mes Concepts</title>
    <link rel="stylesheet" href="../../assets/style/myConcepts.css">
    <script src="../../assets/scripts/myConcepts.js"></script>
</head>
    <body>
        <header>
            <h1>Terminologio</h1>
            <div>
                <a href="../edit/addConcept.php">Ajouter</a>
                <a href="../../index.php">Accueil</a>
            </div>
        </header>
    <div id="concepts">
        <?php for ($i = 0; $i < count($dataConcepts);++$i) {
            echo "<div id=\"concept". $dataConcepts[$i]['id'] ."\" class=\"concept\">
            <script>getImage(". $dataConcepts[$i]['id'] .")</script>
            <p id=\"name". $dataConcepts[$i]["id"] ."\" class=\"name\">" . $dataConcepts[$i]['name'] . "</p>
            <div id=\"image" . $dataConcepts[$i]["id"] . "\" class=\"image\">
                <img id=\"img". $dataConcepts[$i]["id"] ."\" src=\"\" alt=\"" . $dataConcepts[$i]['name'] . "\" height=\"250\" width=\"250\">
            </div>
            <div id=\"links" . $dataConcepts[$i]["id"] . "\" class=\"links\">
                <a href=\"../edit/editConcept.php?id=" . $dataConcepts[$i]["id"] . "\">Modifier</a>
                <a href=\"viewConcept.php?id=" . $dataConcepts[$i]["id"] . "\">Voir</a>
            </div>
        </div>";
        }?>

    </div>
    </body>
<script src="../../assets/scripts/myConcepts.js"></script>
</html>