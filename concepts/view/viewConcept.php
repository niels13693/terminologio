<?php

    error_reporting(E_ALL);
    ini_set('display_errors', 'On');

    use utils\server\Connectdb;
    require("../../utils/server/Connectdb.php");

    //Recupération de l'id du concept:
    $connect = Connectdb::log("../../");
    $id = $_GET["id"];

    //Récuperation du concept:
    $getConcept = $connect->prepare("SELECT name,user_mail FROM concept WHERE id = :id");
    $getConcept->bindParam(":id",$id,PDO::PARAM_INT);
    $resConcept = $getConcept->execute();
    if (!$resConcept) {
        echo 'query execution error';
        die();
    }
    $dataConcept = $getConcept->fetch(PDO::FETCH_ASSOC);
    $name = $dataConcept["name"];
    $mail = $dataConcept["user_mail"];

    //Récuperation des terminologies:
    $getTerms = $connect->prepare("SELECT description,x_coord,y_coord,x_number_coord,y_number_coord
                                        FROM terminology
                                        WHERE user_mail = :mail and concept_name = :name");
    $getTerms->bindParam(":mail",$mail);
    $getTerms->bindParam(":name",$name);
    $resTerms = $getTerms->execute();
    if (!$resTerms) {
        echo 'query execution error';
        die();
    }
    $dataTerms = $getTerms->fetchAll(PDO::FETCH_ASSOC);
    if (isset($_COOKIE["login"])) {
        $cookie_id = $_COOKIE['login'];
    }
    if(isset($cookie_id)) {
        $getUserMail = $connect->prepare('SELECT mail FROM user WHERE cookie_id = :cookie_id');
        $getUserMail->bindParam(':cookie_id',$cookie_id);
        $resUserMail = $getUserMail->execute();
        if (!$resUserMail) {
            echo 'query execution error';
            die();
        }
        $dataMail = $getUserMail->fetch(PDO::FETCH_ASSOC);
        $user_mail = $dataMail["mail"];
    }
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title>Terminologio - Vue</title>
        <link rel="stylesheet" href="../../assets/style/viewConcept.css">
    </head>
    <body>
        <header>
            <h1>Terminologio</h1>
            <div>
                <?php
                    if (isset($user_mail) and $user_mail == "admin@terminologio") {
                        echo "<a href='../../users/ban.php?id=" . $id . "' class='admin'>Bannir l'utilisateur</a>";
                        echo "<a href='../database/delete.php?id=" . $id . "' class='admin'>Supprimer le concept</a>";
                    }
                ?>
                <a href="../../index.php">Accueil</a>
            </div>
        </header>
        <div>
            <div id="column1">
                <div id="displayName">
                    <p id="conceptName"><?php echo $name?></p>
                </div>
                <div id="displayImage">
                    <!--<canvas id="image"></canvas>-->
                    <svg id="svg" width="500" height="500"></svg>
                </div>
            </div>
            <div id="displayTerm">
                <?php
                    for ($i = 0; $i < count($dataTerms);++$i) {
                        echo "<div id=\"term" . ($i + 1) . "\" class=\"term\">
                                  <p>" . ($i + 1) . ": " . $dataTerms[$i]['description'] . "</p>
                                  <p id=\"x_coord" . ($i + 1) . "\" hidden>" . $dataTerms[$i]['x_coord'] . "</p>
                                  <p id=\"y_coord" . ($i + 1) . "\" hidden>" . $dataTerms[$i]['y_coord'] . "</p>
                                  <p id=\"x_number_coord" . ($i + 1) . "\" hidden>" . $dataTerms[$i]['x_number_coord'] . "</p>
                                  <p id=\"y_number_coord" . ($i + 1) . "\" hidden>" . $dataTerms[$i]['y_number_coord'] . "</p>
                              </div>";
                    }
                ?>
            </div>
        </div>
    </body>
    <script type="module" src="../../assets/scripts/viewConcept.js"></script>
</html>