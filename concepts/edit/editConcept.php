<?php

error_reporting(E_ALL);
ini_set('display_errors', 'On');

use utils\server\Cookies;
require("../../utils/server/Cookies.php");


    $connect = Cookies::verifyConnection(true,"../../");
    $mail = $_COOKIE["mail"];

    //Vérification de la propriété du concept par l'utilisateur:
    $conceptId = $_GET["id"];
    $getConcept = $connect->prepare("SELECT user_mail,name FROM concept WHERE id = :id");
    $getConcept->bindParam(":id",$conceptId,PDO::PARAM_INT);
    $resConcept = $getConcept->execute();
    if (!$resConcept) {
        echo 'query execution error';
        die();
    }
    $dataConcept = $getConcept->fetch(PDO::FETCH_ASSOC);
    $conceptMail = $dataConcept["user_mail"];
    if ($conceptMail != $mail) {
        header("Location : ../../index.php");
        exit;
    }
    $conceptName = $dataConcept["name"];

    //Récuperation des terminologies:
    $getTerms = $connect->prepare("SELECT description,x_coord,y_coord,x_number_coord,y_number_coord 
                                        FROM terminology WHERE user_mail = :mail AND concept_name = :name");
    $getTerms->bindParam(":mail",$conceptMail);
    $getTerms->bindParam(":name",$conceptName);
    $resTerms = $getTerms->execute();
    if (!$resTerms) {
        echo 'query execution error';
        die();
    }
    $dataTerms = $getTerms->fetchAll(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Terminologio - Edition</title>
    <link rel="stylesheet" href="../../assets/style/editConcept.css">
</head>
    <body>
        <header>
            <h1>Terminologio</h1>
            <div>
                <a href="../../index.php">Accueil</a>
            </div>
        </header>
        <div>
            <form action="../database/edit.php?id" method="GET">
                <div id="column1">
                    <div id="displayName">
                        <p><?php echo $conceptName;?></p>
                        <input type="hidden" name="conceptName" value="<?php echo $conceptName;?>">
                    </div>
                    <div id="displayImage">
                        <p id="instructions"></p>
                        <div id="svgImage">
                            <!--<canvas id="canvasElement"></canvas>-->
                            <svg id="svg" width="500" height="500"></svg>
                        </div>
                    </div>
                </div>
                <div id="column2">
                    <div id="termList">
                        <?php
                            for ($i = 1;$i <= count($dataTerms);++$i) {
                                echo "<div id=\"term" . $i . "\" class=\"term\">
                                        <label for=\"desc" . $i . "\" id='label" . $i . "'>" . $i . "</label>
                                        <input type=\"text\" id=\"desc" . $i . "\" name='descriptionTerm" . $i . "'
                                        placeholder=\"Description de la terminologie " . $i . "\"
                                        value=\"" . $dataTerms[$i - 1]['description'] . "\" required>
                                        <button id=\"deleteButton" . $i . "\" type=\"button\" class=\"deleteButton\">x</button>
                                        <input type=\"hidden\" id=\"x_coord" . $i . "\" 
                                        value=\"" . $dataTerms[$i - 1]['x_coord'] . "\" name='x_coord" . $i . "'>
                                        <input type=\"hidden\" id=\"y_coord" . $i . "\" 
                                        value=\"" . $dataTerms[$i - 1]['y_coord'] . "\" name='y_coord" . $i . "'>
                                        <input type=\"hidden\" id=\"x_number_coord" . $i . "\" 
                                        value=\"" . $dataTerms[$i - 1]['x_number_coord'] . "\" name='x_number_coord" . $i . "'>
                                        <input type=\"hidden\" id=\"y_number_coord" . $i . "\" 
                                        value=\"" . $dataTerms[$i - 1]['y_number_coord'] . "\" name='y_number_coord" . $i . "'>
                                      </div>";
                            }
                        ?>
                        <button id="addButton" type="button">+</button>
                    </div>
                    <div id="buttons">
                        <input type="hidden" name="termNumber" value="0" id="termNumber">
                        <input type="hidden" name="id" value="<?php echo $conceptId;?>">
                        <div id="delete">
                            <a href="../database/delete.php?id=<?php echo $conceptId ;?>">Supprimer</a>
                        </div>
                        <div id="save">
                            <input type="submit" value="Sauvegarder" id="saveButton">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </body>
<script type="module" src="../../assets/scripts/editConcept.js"></script>
</html>