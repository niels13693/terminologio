<?php

error_reporting(E_ALL);
ini_set('display_errors', 'On');

use utils\server\Cookies;
require("../../utils/server/Cookies.php");

Cookies::verifyConnection(true,"../../");
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Terminologio - Ajout</title>
    <link rel="stylesheet" href="../../assets/style/addConcept.css">
</head>
<body>
<header>
    <h1>Terminologio</h1>
    <div>
        <a href="../../index.php">Accueil</a>
    </div>
</header>
<div>
    <form action="../database/add.php" method="post" enctype="multipart/form-data">
        <div id="column1">
            <div id="divFile">
                <label for="conceptName" hidden></label>
                <input type="text" name="conceptName" placeholder="Nom du concept" maxlength="20" minlength="2" id="conceptName">
                <input type="file" name="conceptImage" accept="image/*" id="fileSelector">
                <p id="fileError"></p>
            </div>
            <div id="displayImage">
                <p id="instructions"></p>
                <!--<div id="canvasImage"><canvas id="canvaElement"></canvas></div>-->
                <div id="svgDiv">
                    <svg width="500" height="500" id="svg"></svg>
                </div>
            </div>
        </div>
        <div id="column2">
            <div id="termList">
                <button id="addButton" type="button">+</button>
            </div>
            <div id="save">
                <input type="hidden" name="termNumber" value="0" id="termNumber">
                <input type="submit" value="Sauvegarder" id="submit">
            </div>
        </div>
    </form>
</div>
<script type="module" src="../../assets/scripts/addConcept.js"></script>
</body>
</html>