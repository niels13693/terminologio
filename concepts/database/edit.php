<?php

error_reporting(E_ALL);
ini_set('display_errors', 'On');

use utils\server\Connectdb;
require("../../utils/server/Connectdb.php");

//Récuperation de l'ID:

$id = $_GET["id"];

//Connection à la bdd:

$connect = Connectdb::log("../../");

//Mise a jour des terminologies:

$termNumber = $_GET["termNumber"];
$name = $_GET["conceptName"];
$mail = $_COOKIE["mail"];

//Suppresion des anciennes terminologies;

$deleteOldTerm = $connect->prepare("DELETE FROM terminology WHERE concept_name = :name and user_mail = :mail");
$deleteOldTerm->bindParam(":name",$name);
$deleteOldTerm->bindParam(":mail",$mail);
$resDelete = $deleteOldTerm->execute();
if (!$resDelete) {
    echo 'query execution error';
    die();
}

for ($i = 1;$i <= $termNumber;++$i) {
    //Récuperation des données:
    $desc = $_GET["descriptionTerm" . $i];
    $x1 = $_GET["x_coord" . $i];
    $y1 = $_GET["y_coord" . $i];
    $x2 = $_GET["x_number_coord" . $i];
    $y2 = $_GET["y_number_coord" . $i];
    //insertion d'une nouvelle terminologie:
    $insertTerm = $connect->prepare("INSERT INTO 
            terminology(description, x_coord, y_coord, x_number_coord, y_number_coord, concept_name, user_mail) 
            VALUES (:desc,:x1,:y1,:x2,:y2,:name,:mail)");
    $insertTerm->bindParam(":desc",$desc);
    $insertTerm->bindParam(":x1",$x1);
    $insertTerm->bindParam(":y1",$y1);
    $insertTerm->bindParam(":x2",$x2);
    $insertTerm->bindParam(":y2",$y2);
    $insertTerm->bindParam(":name",$name);
    $insertTerm->bindParam(":mail",$mail);
    $resInsert = $insertTerm->execute();
    if (!$resInsert) {
        echo 'query execution error';
        die();
    }
}



/*
    //Vérification de la présence des données:
    $checkTerm = $connect->prepare("SELECT * FROM terminology WHERE
                              user_mail = :mail and concept_name = :name and x_coord = :x and y_coord = :y");
    $checkTerm->bindParam(":mail",$mail);
    $checkTerm->bindParam(":name",$name);
    $checkTerm->bindParam(":x",$x1);
    $checkTerm->bindParam(":y",$y1);
    $resCheck = $checkTerm->execute();
    if (!$resCheck) {
        echo 'query execution error';
        die();
    }
    if ($checkTerm->rowCount() > 0) {
        //Mise à jour d'une terminologie:
        $updateTerm = $connect->prepare("UPDATE terminology SET description = :desc WHERE 
                                               x_coord = :x1 and y_coord = :y1 and user_mail = :mail and 
                                               concept_name = :name");
        $updateTerm->bindParam(":desc",$desc);
        $updateTerm->bindParam(":x1",$x1);
        $updateTerm->bindParam(":y1",$y1);
        $updateTerm->bindParam(":mail",$mail);
        $updateTerm->bindParam(":name",$name);
        $resUpdate = $updateTerm->execute();
        if (!$resUpdate) {
            echo 'query execution error';
            die();
        }
    } else {


}*/

header("Location: ../view/viewConcept.php?id=" . $id);
exit;