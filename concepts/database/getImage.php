<?php

error_reporting(E_ALL);
ini_set('display_errors', 'On');

use utils\server\Connectdb;
require("../../utils/server/Connectdb.php");

//Récuperation de l'ID:
$id = $_REQUEST['id'];

//Connection à la bdd:

$connect = Connectdb::log("../../");

//Récuperation de l'image:

$getImage = $connect->prepare("SELECT image FROM concept WHERE id = :id");
$getImage->bindParam(":id",$id,PDO::PARAM_INT);
$resImage = $getImage->execute();
if (!$resImage) {
    echo 'query execution error';
    die();
}

$imagePath = $getImage->fetch(PDO::FETCH_ASSOC)["image"];
$imageType = pathinfo($imagePath, PATHINFO_EXTENSION);
$image = file_get_contents($imagePath);
$imageBase64 = "data:image/" . $imageType . ";base64," . base64_encode($image);

echo $imageBase64;
exit;