<?php

error_reporting(E_ALL);
ini_set('display_errors', 'On');

use utils\server\Connectdb;
require("../../utils/server/Connectdb.php");

//Récuperation de l'ID:
$id = $_GET["id"];

//Connexion à la bdd:
$connect = Connectdb::log("../../");

//Suppression du concept:
$deleteConcept = $connect->prepare("DELETE FROM concept WHERE id = :id");
$deleteConcept->bindParam(":id",$id,PDO::PARAM_INT);
$resDelete = $deleteConcept->execute();
if (!$resDelete) {
    echo 'query execution error';
    die();
}

//Retour au site:
header("Location: ../../index.php");
exit;