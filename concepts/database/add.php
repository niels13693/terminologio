<?php

error_reporting(E_ALL);
ini_set('display_errors', 'On');

use utils\server\Connectdb;
require("../../utils/server/Connectdb.php");


//Connection à la bdd:
$connect = Connectdb::log("../../");

//Ajout du concept

$conceptName = $_POST["conceptName"];
$mail = $_COOKIE["mail"];
$image = "";

$addConcept = $connect->prepare("INSERT INTO concept(name,user_mail,image) VALUES (:name,:mail,:image)");
$addConcept->bindParam(":name",$conceptName);
$addConcept->bindParam(":mail",$mail);
$addConcept->bindParam(":image",$image);
$resConcept = $addConcept->execute();
if (!$resConcept) {
    echo 'query execution error';
    die();
}

$getId = $connect->prepare("SELECT id FROM concept WHERE user_mail = :mail and name = :name");
$getId->bindParam(":mail",$mail);
$getId->bindParam(":name",$conceptName);
$resId = $getId->execute();
if (!$resId) {
    echo 'query execution error';
    die();
}
$id = $getId->fetch(PDO::FETCH_ASSOC)["id"];
$imageFileName = basename($_FILES['conceptImage']['name']);
$imageExt =  explode('.',$imageFileName)[1];
$imagePath = "../images/" . $id . "." . $imageExt;
if (!move_uploaded_file($_FILES['conceptImage']["tmp_name"],$imagePath)) {
    print_r($_FILES);
    die();
}

$addImage = $connect->prepare("UPDATE concept SET image = :imagePath WHERE id = :id");
$addImage->bindParam(":imagePath",$imagePath);
$addImage->bindParam(":id",$id,PDO::PARAM_INT);
$resImage = $addImage->execute();
if (!$resImage) {
    echo 'query execution error';
    die();
}

//Ajout des terminologies:

$termNumber = $_POST["termNumber"];
for ($i = 1; $i <= $termNumber;++$i) {
    $x_coord = $_POST["x_coord" . $i];
    $y_coord = $_POST["y_coord" . $i];
    $x_number_coord = $_POST["x_number_coord" . $i];
    $y_number_coord = $_POST["y_number_coord" . $i];
    $description = $_POST["descriptionTerm" . $i];
    $addTerm = $connect->prepare("INSERT INTO 
    terminology(description, x_coord, y_coord, x_number_coord, y_number_coord, concept_name, user_mail) 
    VALUES (:desc,:x_coord,:y_coord,:x_number_coord,:y_number_coord,:name,:mail)");
    $addTerm->bindParam(":desc",$description);
    $addTerm->bindParam(":x_coord",$x_coord);
    $addTerm->bindParam(":y_coord",$y_coord);
    $addTerm->bindParam(":x_number_coord",$x_number_coord);
    $addTerm->bindParam(":y_number_coord",$y_number_coord);
    $addTerm->bindParam(":name",$conceptName);
    $addTerm->bindParam(":mail",$mail);
    $resTerm = $addTerm->execute();
    if (!$resTerm) {
        echo 'query execution error';
        die();
    }
}

header("Location: ../../index.php");
exit;