function getImage(id) {
    const request = new XMLHttpRequest();
    request.open("GET","../../concepts/database/getImage.php?id=" + id);
    request.onreadystatechange = function () {
        if (request.status === 200 && request.readyState === 4) {
            var imageEncoded = request.responseText;
            document.getElementById("img" + id).src = imageEncoded;
        }
    }
    request.send();
}