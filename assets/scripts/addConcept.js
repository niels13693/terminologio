import Concept from "../../utils/local/Concept.js";
import CookieTools from "../../utils/local/CookieTools.js";
import ConceptTools from "../../utils/local/ConceptTools.js";
import Terminology from "../../utils/local/Terminology.js";

const concept = new Concept("default");
document.getElementById("conceptName").addEventListener("input",async (e) => {
    const mail = CookieTools.parseCookieValue("mail");
    if (mail == null) {
        throw new Error("invalid cookie mail");
    }
    const name = e.target.value;
    const isPresent = await ConceptTools.verifyNameOnDB(name, mail, "../../");
    if (isPresent) {
        document.getElementById("fileError").innerText = "Vous avez déjà un concept nommé de cette facon.";
        const disabled = document.createAttribute("disabled");
        document.getElementById("submit").setAttributeNode(disabled);
    } else {
        document.getElementById("fileError").innerText = null;
        document.getElementById("submit").removeAttribute("disabled");
    }
});

document.getElementById("fileSelector").addEventListener("change", async (e) => {
    await concept.loadImageFromFile(e.target);
    const dest = document.getElementById("svgDiv");
    dest.removeChild(document.getElementById("svg"));
    concept.drawImage(dest);
});

document.getElementById("addButton").addEventListener("click", async () => {
    const svg = document.getElementById("svg");
    //Désactivation du bouton submit pendant l'enregistrement de la terminologie:
    const disabled = document.createAttribute("disabled");
    document.getElementById("submit").setAttributeNode(disabled);
    //Création de la terminologie:
    const termNumber = document.getElementsByClassName("term").length + 1;
    const term = new Terminology(termNumber);
    //Selection du premier point:
    document.getElementById("instructions").innerText = "Sélectionnez l'endroit de la terminologie";
    await term.setFirstPoint(svg);
    //Selection du second point;
    document.getElementById("instructions").innerText = "Sélectionnez l'endroit du numéro de la terminologie";
    await term.setSecondPoint(svg);
    document.getElementById("instructions").innerText = null;
    //Dessin de la ligne de la terminologie:
    term.draw(svg);
    term.drawId(svg);
    //Ajout de la terminologie:
    concept.addTerm(termNumber,term,document.getElementById("termList"));
    document.getElementById("termNumber").value = document.getElementsByClassName("term").length;
    //Re-activation du bouton submit:
    document.getElementById("submit").removeAttribute("disabled");
} );