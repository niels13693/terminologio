import Concept from "../../utils/local/Concept.js";

const concept = new Concept("");
//Récuperation de l'ID:
const url = window.location.href;
const id = Number(url.substring(url.indexOf('?')).split('=')[1]);
await concept.loadImageFromServer(id,"../../");
const container = document.getElementById("displayImage");
container.removeChild(document.getElementById("svg"));
concept.drawImage(container);
concept.drawLinesFromHTML(document.getElementById("svg"),"p");