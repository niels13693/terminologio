import Concept from "../../utils/local/Concept.js";
import Terminology from "../../utils/local/Terminology.js";
import CookieTools from "../../utils/local/CookieTools.js";
import ConceptTools from "../../utils/local/ConceptTools.js";

const concept = new Concept("");
//Récuperation de l'ID:
const url = window.location.href;
const id = Number(url.substring(url.indexOf('?')).split('=')[1]);
await concept.loadImageFromServer(id,"../../");
const container = document.getElementById("svgImage");
container.removeChild(document.getElementById("svg"));
concept.drawImage(container);
concept.drawLinesFromHTML(document.getElementById("svg"),"input");
const max = document.getElementsByClassName("term").length;
for (let i = 1;i <= max;++i) {
    const button = document.getElementById("deleteButton" + i);
    button.addEventListener("click",(e) => {
       const termId = Number(e.target.id.charAt(e.target.id.length - 1));
       concept.removeTerm(termId,document.getElementById("termList"),document.getElementById("svg"));
       document.getElementById("termNumber").value = document.getElementsByClassName("term").length;
    });
}
document.getElementById("termNumber").value = max;

document.getElementById("addButton").addEventListener("click", async (e) => {
    const svg = document.getElementById("svg");
    //Désactivation du bouton submit pendant l'enregistrement de la terminologie:
    const disabled = document.createAttribute("disabled");
    document.getElementById("saveButton").setAttributeNode(disabled);
    //Création de la terminologie:
    const termNumber = document.getElementsByClassName("term").length + 1;
    const term = new Terminology(termNumber);
    //Selection du premier point:
    document.getElementById("instructions").innerText = "Sélectionnez l'endroit de la terminologie";
    await term.setFirstPoint(svg);
    //Selection du second point;
    document.getElementById("instructions").innerText = "Sélectionnez l'endroit du numéro de la terminologie";
    await term.setSecondPoint(svg);
    document.getElementById("instructions").innerText = null;
    //Dessin de la ligne de la terminologie:
    term.draw(svg);
    term.drawId(svg);
    //Ajout de la terminologie:
    concept.addTerm(termNumber, term, document.getElementById("termList"));
    document.getElementById("termNumber").value = document.getElementsByClassName("term").length;
    //Re-activation du bouton submit:
    document.getElementById("saveButton").removeAttribute("disabled");
});
