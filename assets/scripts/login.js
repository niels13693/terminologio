function getPHPError() {
    const url = window.location.href;
    if (url.includes('?')) {
        const start = url.indexOf('?') + 1;
        const error = url.substring(start).split('=')[1];
        if (error === "not_found") {
            document.getElementById("display_error").innerHTML = "E-mail inconnu";
        } else if (error === "false") {
            document.getElementById("display_error").innerHTML = "Mot de passe incorrect";
        }
    }
}