function validate_password() {
    const pwd = document.getElementById("password1").value;
    const confirm = document.getElementById("password2").value;
    console.log(pwd);
    console.log(confirm);
    const submit = document.getElementById("submit");
    if (pwd !== confirm) {
        document.getElementById("display_error").innerHTML = "Confirmation différente";
        const disabled = document.createAttribute('disabled');
        submit.setAttributeNode(disabled);
    } else {
        document.getElementById("display_error").innerHTML = "";
        submit.removeAttribute('disabled');
    }
}

function getPHPError() {
    const url = window.location.href;
    if(url.includes('?')) {
        const start = url.indexOf('?') + 1;
        const error = url.substring(start).split('=')[1];
        if (error === "found") {
            document.getElementById("display_error").innerHTML = "Mail déja utilisé";
        } else if (error === "true") {
            document.getElementById("display_error").innerHTML = "Vous avez été banni(e)";
        }
    }
}